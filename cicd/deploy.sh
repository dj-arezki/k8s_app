#!/bin/bash

: '
The script is made to automate the deployment.

The script should be launched like this :

./deploy.sh -e <environment> -t <TAG>

-e | --environment                    Environment (choose instance)
-t | --tag                            TAG (example: -t nexus)

'

OPTS=`getopt -n 'parse-options' -o he:t: --long help,environment:tag:  -- "$@"`

if [[ "$?" != 0 ]]; then
    echo "Failed parsing options." >&2
    exit 1
fi

eval set -- "$OPTS"

function usage(){
    echo "Usage:"
    echo "   -h | --help                            Display usage"
    echo "   -e | --environment                     Environment (possible values: 'local', 'dev', 'pfv', 'prod')"
    exit 0
}

while true; do
  case "$1" in
    -h  | --help ) usage; shift;;
    -e  | --environment ) ENVIRONMENT="$2"; shift 2;;
    -t  | --tag ) TAG="$2"; shift 2;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

# Infer host variables
if [[ ${ENVIRONMENT} = "instance1" ]] ; then
    REMOTE_HOST="inst1"
elif [[ ${ENVIRONMENT} = "instance2" ]] ; then
    REMOTE_HOST="inst2"
elif [[ ${ENVIRONMENT} = "instance2" ]] ; then
    REMOTE_HOST="inst3"
else
    echo "Environment variable $ENVIRONMENT unknown" && exit 1
fi



echo "REMOTE_HOST=${REMOTE_HOST}"

# Configure Ansible
export ANSIBLE_HOST_KEY_CHECKING=False
export ANSIBLE_COMMAND_WARNINGS=False
export SSH_COMMON_ARGS="-oHostKeyAlgorithms=+ssh-dss"

ansible-playbook -i cicd/ansible/hosts  --become-user ec2-user --diff cicd/ansible/platform.yml -e "host=$REMOTE_HOST" -t $TAG