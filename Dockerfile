FROM python:3

WORKDIR /app
COPY . .

#execute diff cmd
RUN pip install flask
CMD ["python3", "src/app-flask.py" ]
# To build the image and name it hello-world
# docker build -t hello-world install-apps/
#to run the container and expose it on the port 5001, -d: in background
#docker run hello-world:latest
